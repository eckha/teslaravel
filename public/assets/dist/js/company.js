$(function() {
    $('#datashow').DataTable({
        processing: true,
        serverSide: true,
        ajax: "company",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'logo', name: 'logo'},
            {data: 'website', name: 'website'},
            {
                data: 'action', 
                name: 'action', 
                orderable: true, 
                searchable: true
            },
        ]
    });

});

$(document).on("click", "#btnEditCompany", function() {
    var id = $(this).attr("data-id");
    $.ajax({
        type: "GET",
        url: "company/edit/"+id,
        dataType: "json",
        success: function(data) {
            var data = data.company;
            $('#editCompany').modal('show');
            $('#formEditCompany').attr('action', 'company/update/'+data.id);
            $('#name').val(data.name);
            $('#website').val(data.website);
            $('#email').val(data.email);
        }
    });
});



$(document).on("click", "#btnDeleteCompany", function() {
    var id = $(this).attr("data-id");
    Swal.fire({
        title: 'Delete Company Data !',
        text: "Are you sure to delete this company data? ?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
    }).then((result) => {
        if(result.value) {
            $.ajax({
                url: "company/delete/"+id,
                dataType: "json",
                success: function(data) {
                    debugger;
                    var msg = data.msg;
                    switch(msg)
                    {
                        case 'OK':
                            toastr.success('Success, Company has been deleted.');
                            debugger;
                            $('#datashow').DataTable().ajax.reload();
                        break;
                        case 'NotFound':
                            toastr.error('Failed, Company not found.');
                            $('#datashow').DataTable().ajax.reload();
                        break;
                        default:
                            toastr.error('Failed, ', msg);
                            $('#datashow').DataTable().ajax.reload();
                    }
                }
            });
        }
    });
});