load_data();

function load_data() {
    $('#dataEmployee').DataTable({
        processing: true,
        serverSide: true,
        ajax: "employee",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'full_name', name: 'full_name'},
            {data: 'company', name: 'company'},
            {data: 'email', name: 'email'},
            {data: 'phone', name: 'phone'},
            {
                data: 'action', 
                name: 'action', 
                orderable: true, 
                searchable: true
            },
        ]
    });
}


$(document).on("click", "#btnShowCompany", function() {
    var id = $(this).attr("data-company-id");
    $.ajax({
        type: "GET",
        url: "company/show/"+id,
        dataType: "json",
        success: function(datas) {
            var data = datas.company;
            document.getElementById('companyName').innerHTML=data.name;
            document.getElementById('companyWebsite').innerHTML=data.website;
            document.getElementById('companyEmail').innerHTML=data.email;
            $("#companyLogo").attr("src", "images/company/"+data.logo);
            $('#detailCompany').modal('show');
        }
    });
});

$(document).on("click", "#btnEditEmployee", function() {
    var id = $(this).attr("data-id");
    $.ajax({
        type: "GET",
        url: "employee/edit/"+id,
        dataType: "json",
        success: function(datas) {
            var data = datas.employee;
            var data2 = datas.company;
            
            var option_company = '<option value="">Company</optin>';
            $('#editEmployee').modal('show');
            $('#formEditEmployee').attr('action', 'employee/update/'+data.id);
            $('#first_name').val(data.first_name);
            $('#last_name').val(data.last_name);
            $('#email').val(data.email);
            $('#phone').val(data.phone);

            for(var i=0; i<data2.length; i++) {
                if (data2[i].id==data.company_id) {
                    option_company +='<option value="'+data2[i].id+'" selected>'+data2[i].name+'</option>';
                } else {
                    option_company +='<option value="'+data2[i].id+'">'+data2[i].name+'</option>';
                }
            }

            $("#company_id").html(option_company);
        }
    });
});



$(document).on("click", "#btnDeleteEmployee", function() {
    var id = $(this).attr("data-id");
    Swal.fire({
        title: 'Delete Employee Data !',
        text: "Are you sure to delete this employee data? ?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
    }).then((result) => {
        if(result.value) {
            $.ajax({
                url: "employee/delete/"+id,
                dataType: "json",
                success: function(data) {
                    var msg = data.msg;
                    switch(msg)
                    {
                        case 'OK':
                            toastr.success('Success, Employee has been deleted.');
                            debugger;
                            $('#dataEmployee').DataTable().ajax.reload();
                        break;
                        case 'NotFound':
                            toastr.error('Failed, Employee not found.');
                            $('#dataEmployee').DataTable().ajax.reload();
                        break;
                        default:
                            toastr.error('Failed, ', msg);
                            $('#dataEmployee').DataTable().ajax.reload();
                    }
                }
            });
        }
    });
});