<?php

return [

    "defaults" => [
        "guard" => "user",
        "passwords" => "user",
    ],

    "guards" => [
        "user" => [
            "driver" => "session",
            "provider" => "user",
        ],
    ],

    "providers" => [
        "user" => [
            "driver" => "eloquent",
            "model" => App\Http\Models\User::class,
        ],
    ],

    "passwords" => [
        "user" => [
            "provider" => "user",
            "table" => "password_resets",
            "expire" => 60,
            "throttle" => 60,
        ],
    ],

    "password_timeout" => 10800,

];
