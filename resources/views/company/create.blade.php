@extends("layout")

@section("name", $name)
@section("icon", $icon)
@section("title", $title)

@section("content")
	
	<div class="content-header">
      	<div class="container-fluid">
        	<div class="row mb-2">
          		<div class="col-sm-6">
            		<h4 class="m-0">@yield("title")</h4>
          		</div><!-- /.col -->
          		<div class="col-sm-6">
            		<ol class="breadcrumb float-sm-right">
              			<li class="breadcrumb-item"><a href="">@yield("name")</a></li>
              			<li class="breadcrumb-item active">@yield("title")</li>
            		</ol>
          		</div><!-- /.col -->
        	</div><!-- /.row -->
      	</div><!-- /.container-fluid -->
    </div>

    <section class="content">
      	<div class="container-fluid">

          <div class="row">
            <div class="col-12">
              <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Company Data Form</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" enctype="multipart/form-data"  method="post" action="{{ route("$slug.store") }}">
                @csrf
                <div class="card-body">

                  <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label @error('name') text-danger @enderror">Name</label>
                    <div class="col-sm-10">
                      <input type="text" name="name" class="form-control @error ('name') is-invalid @enderror" id="name" placeholder="Name" value="{{ old('name') }}" autofocus>
                      @error("name")
                        <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label @error('email') text-danger @enderror">Email</label>
                    <div class="col-sm-10">
                      <input type="email" name="email" class="form-control @error ('email') is-invalid @enderror" id="email" placeholder="Email" value="{{ old('email') }}">
                      @error("email")
                        <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="website" class="col-sm-2 col-form-label @error('website') text-danger @enderror">Website</label>
                    <div class="col-sm-10">
                      <input type="text" name="website" class="form-control @error ('website') is-invalid @enderror" id="website" placeholder="Website" value="{{ old('website') }}">
                      @error("website")
                        <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="customFile" class="col-sm-2 col-form-label @error('logo') text-danger @enderror">Logo</label>
                    <div class="col-sm-10">
                      <div class="input-group">
                        <div class="custom-file">
                          <input type="file" class="custom-file-input @error ('logo') is-invalid @enderror" id="customFile" name="logo">
                          <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>

                      </div>
                      @error("logo")
                        <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>

                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-info">Save</button>
                  <button type="reset" class="btn btn-default">Reset</button>
                </div>
                <!-- /.card-footer -->
              </form>
            </div>
            </div>
            </div>
          </div>
      	</div>
    </section>

 @endsection