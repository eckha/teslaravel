@extends("layout")

@section("name", $name)
@section("icon", $icon)
@section("title", $title)



@section("content")
	
	<div class="content-header">
      	<div class="container-fluid">
          @include("alert")
        	<div class="row mb-2">
          		<div class="col-sm-6">
            		<h4 class="m-0">@yield("title")</h4>
          		</div><!-- /.col -->
          		<div class="col-sm-6">
            		<ol class="breadcrumb float-sm-right">
              			<li class="breadcrumb-item"><a href="">@yield("name")</a></li>
              			<li class="breadcrumb-item active">@yield("title")</li>
            		</ol>
          		</div><!-- /.col -->
        	</div><!-- /.row -->
      	</div><!-- /.container-fluid -->
    </div>

    <section class="content">
      	<div class="container-fluid">

          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <a href="{{ url('company/create') }}" title="" class="btn btn-sm btn-primary"><i class="fas fa-plus"></i> Add Data</a>
                </div>
                <div class="card-body">
                  <table  class="table table-bordered table-hover" id="datashow">
                    <thead>
                    <tr class="text-center">
                      <th>No</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Logo</th>
                      <th>Website</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>

            <div class="modal fade" id="editCompany">
              <div class="modal-dialog modal-lg ">

                <form class="form-horizontal" enctype="multipart/form-data"  method="post" id="formEditCompany" action="">
                  @csrf

                  <div class="modal-content">

                    <div class="modal-header bg-info">
                      <h4 class="modal-title">Edit Data</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>

                    <div class="modal-body">
                      
                      <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label @error('name') text-danger @enderror">Name</label>
                        <div class="col-sm-10">
                          <input type="text" name="name" class="form-control @error ('name') is-invalid @enderror" id="name" placeholder="Name" value="{{ old('name') }}" autofocus>
                          @error("name")
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="email" class="col-sm-2 col-form-label @error('email') text-danger @enderror">Email</label>
                        <div class="col-sm-10">
                          <input type="email" name="email" class="form-control @error ('email') is-invalid @enderror" id="email" placeholder="Email" value="">
                          @error("email")
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="website" class="col-sm-2 col-form-label @error('website') text-danger @enderror">Website</label>
                        <div class="col-sm-10">
                          <input type="text" name="website" class="form-control @error ('website') is-invalid @enderror" id="website" placeholder="Website" value="">
                          @error("website")
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="customFile" class="col-sm-2 col-form-label @error('logo') text-danger @enderror">Logo</label>
                        <div class="col-sm-10">
                          <div class="input-group">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input @error ('logo') is-invalid @enderror" id="customFile" name="logo">
                              <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>

                          </div>
                          @error("logo")
                            <div class="invalid-feedback">{{ $message }}</div>
                          @enderror
                        </div>
                      </div>


                    </div>

                    <div class="modal-footer">
                      <button type="submit" class="btn btn-primary">Update</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>

                  </div>

                </form>


              </div>
            </div>
          </div>
      	</div>
    </section>

 @endsection

