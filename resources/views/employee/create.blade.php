@extends("layout")

@section("name", $name)
@section("icon", $icon)
@section("title", $title)

@section("content")
	
	<div class="content-header">
      	<div class="container-fluid">
        	<div class="row mb-2">
          		<div class="col-sm-6">
            		<h4 class="m-0">@yield("title")</h4>
          		</div><!-- /.col -->
          		<div class="col-sm-6">
            		<ol class="breadcrumb float-sm-right">
              			<li class="breadcrumb-item"><a href="">@yield("name")</a></li>
              			<li class="breadcrumb-item active">@yield("title")</li>
            		</ol>
          		</div><!-- /.col -->
        	</div><!-- /.row -->
      	</div><!-- /.container-fluid -->
    </div>

    <section class="content">
      	<div class="container-fluid">

          <div class="row">
            <div class="col-12">
              <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Employee Data Form</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" enctype="multipart/form-data"  method="post" action="{{ route("$slug.store") }}">
                @csrf
                <div class="card-body">

                  <div class="form-group row">
                    <label for="first_name" class="col-sm-2 col-form-label @error('first_name') text-danger @enderror">First Name</label>
                    <div class="col-sm-10">
                      <input type="text" name="first_name" class="form-control @error ('first_name') is-invalid @enderror" id="first_name" placeholder="First Name" value="{{ old('first_name') }}" autofocus>
                      @error("first_name")
                        <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="last_name" class="col-sm-2 col-form-label @error('last_name') text-danger @enderror">Last Name</label>
                    <div class="col-sm-10">
                      <input type="text" name="last_name" class="form-control @error ('last_name') is-invalid @enderror" id="last_name" placeholder="Last Name" value="{{ old('last_name') }}">
                      @error("last_name")
                        <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="company_id" class="col-sm-2 col-form-label @error('company_id') text-danger @enderror">Company</label>
                    <div class="col-sm-10">
                      <select class="form-control select2 @error ('company_id') is-invalid @enderror" style="width: 100%;" name="company_id" id="company_id">
                        <option value="">Company</option>
                        @foreach ($data_company as $company)
                            <option value="{{ $company->id }}" >{{ $company->name }}</option>
                        @endforeach
                      </select>
                      @error("company_id")
                        <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label @error('email') text-danger @enderror">Email</label>
                    <div class="col-sm-10">
                      <input type="email" name="email" class="form-control @error ('email') is-invalid @enderror" id="email" placeholder="Email" value="{{ old('email') }}">
                      @error("email")
                        <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="phone" class="col-sm-2 col-form-label @error('phone') text-danger @enderror">Phone</label>
                    <div class="col-sm-10">
                      <input type="text" name="phone" class="form-control @error ('phone') is-invalid @enderror" id="phone" placeholder="Phone" value="{{ old('phone') }}">
                      @error("phone")
                        <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>


                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-info">Save</button>
                  <button type="reset" class="btn btn-default">Reset</button>
                </div>
                <!-- /.card-footer -->
              </form>
            </div>
            </div>
            </div>
          </div>
      	</div>
    </section>

 @endsection