@section("head")
    @include("partial.head")
@endsection

@section("header")
    @include("partial.header")
@endsection

@section("navbar")
    @include("partial.navbar")
@endsection

@section("footer")
    @include("partial.footer")
@endsection



@yield("head")

@yield("header")

@yield("navbar")

@yield("content")

@yield("footer")