<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="" class="brand-link">
      <img src="images/logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">TesLaravel</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          @if(Auth::user()->level == "user")
            <img src="images/user.jpg" class="img-circle elevation-2" alt="User Image">
          @else
            <img src="images/admin.jpg" class="img-circle elevation-2" alt="User Image">
          @endif
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->name }}</a>
        </div>
      </div>

      

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          
          <li class="nav-item">
            <a href="{{ route("index") }}" class="nav-link @if($name=="Dashboard") {{"active"}} @endif">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>Dashboard</p>
            </a>
          </li>

          @if(Auth::user()->level == "admin")
          <li class="nav-item">
            <a href="{{ route("company.index") }}" class="nav-link @if($name=="Company") {{"active"}} @endif">
              <i class="nav-icon fas fa-building"></i>
              <p>Companies</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route("employee.index") }}" class="nav-link @if($name=="Employee") {{"active"}} @endif">
              <i class="nav-icon fas fa-restroom"></i>
              <p>Employees</p>
            </a>
          </li>
          @endif

          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <div class="content-wrapper">