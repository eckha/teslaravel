@if ($errors->any())
    @foreach ($errors->all() as $error)
        <div class="alert alert-danger alert-dismissible">
            {{ $error }}
            <button type="button" class="close" data-bs-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @endforeach
@endif

@if(session()->has("success"))
    <div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <i class="icon fas fa-check"></i>
      {{ session()->get("success") }}
    </div>
@endif

@if(session()->has("danger"))
    <div class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <i class="icon fas fa-ban"></i>
      {{ session()->get("danger") }}
    </div>
@endif