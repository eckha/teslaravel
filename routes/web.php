<?php

use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "login", "as" => "login"], function () {
    $controller = "LoginController";
    Route::any("", $controller . "@index")->name("");
});

Route::group(["middleware" => "auth:user"], function () {

	Route::group(["as" => ""], function () {
        $controller = "DashboardController";
        Route::any("", $controller . "@index")->name("index");
    });

	Route::group(["prefix" => "logout", "as" => "logout."], function () {
        $controller = "LogoutController";
        Route::any("", $controller . "@index")->name("index");
    });

    Route::group(["prefix" => "company", "as" => "company."], function () {
        $controller = "CompanyController";
        Route::any("", $controller . "@index")->name("index");
        Route::any("create", $controller . "@create")->name("create");
        Route::any("store", $controller . "@store")->name("store");
        Route::any("show/{id}", $controller . "@show")->name("show");
        Route::any("edit/{id}", $controller . "@edit")->name("edit");
        Route::any("update/{id}", $controller . "@update")->name("update");
        Route::any("delete/{id}", $controller . "@destroy")->name("destroy");
    });

    Route::group(["prefix" => "employee", "as" => "employee."], function () {
        $controller = "EmployeeController";
        Route::any("", $controller . "@index")->name("index");
        Route::any("create", $controller . "@create")->name("create");
        Route::any("store", $controller . "@store")->name("store");
        Route::any("edit/{id}", $controller . "@edit")->name("edit");
        Route::any("update/{id}", $controller . "@update")->name("update");
        Route::any("delete/{id}", $controller . "@destroy")->name("destroy");
        Route::any("sendNotification/{id}", $controller . "@sendNotification")->name("sendNotification");
    });


});
