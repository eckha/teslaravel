<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = [
        "first_name", "first_name", "company_id", "email", "phone"
    ];

    public function company()
	{
	    return $this->belongsTo(Company::class);
	}

  public function getFullNameAttribute () 
    {
        return $this->first_name . ' ' . $this->last_name;   
    } 

	 
}
