<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use DB;
use Str;

class DashboardController extends Controller
{
    public function page()
    {
        return collect([
            "name" => "Dashboard",
            "icon" => "fas fa-tachometer-alt"
        ]);
    }

    public function index()
    {
        $name   = $this->page()["name"];
        $slug   = Str::slug($this->page()["name"]);
        $icon   = $this->page()["icon"];
        $title = "Dashboard";

       
        return view($slug.'.index', compact('name','slug', 'icon', 'title'));
    }
}
