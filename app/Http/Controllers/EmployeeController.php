<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Models\Employee;
use App\Http\Models\Company;
use Illuminate\Http\Request;
use App\Http\Requests\StoreEmployeeRequest;

use DB;
use Str;
use File;
use DataTables;
use Notification;
use App\Notifications\sendNotification;

class EmployeeController extends Controller
{
    public function page()
    {
        return collect([
            "name" => "Employee",
            "icon" => "fas fa-restroom"
        ]);
    }

    public function index(Request $request)
	{
	    $name = $this->page()["name"];
	    $slug = Str::slug($this->page()["name"]);
	    $icon = $this->page()["icon"];
	    $title = "Employee Data";

	    if (Auth::user()->level != "admin") {
	        return redirect()->route("index")->withDanger("You dont have authorization to access this page");
	    }

	    if ($request->ajax()) {

	    	// $data_employee = DB::table('employees as emp')
	     //                        ->join('companies as cmp', 'cmp.id', '=', 'emp.company_id')
	     //                        ->selectRaw('emp.*, cmp.name as company')
	     //                        ->orderBy('emp.first_name')
	     //                        ->get();

	        $data_employee = Employee::all();

	        return Datatables::of($data_employee)
	            ->addIndexColumn()
	            ->addColumn('company', function ($row) {
                        $link = '<span class="btn-link" id="btnShowCompany" data-company-id="'.$row->company_id.'" style="cursor:pointer;">'.$row->company->name.'</span>';
                        return $link;
                })
	            ->addColumn('full_name', function ($row) {
	                    $full_name = $row->full_name;
	                    return $full_name;
	            })
	            ->addColumn('action', function($row){
	                $actionBtn = '<button id="btnEditEmployee" data-id="'.$row->id.'" class="edit btn btn-success btn-sm">Edit</button> <button id="btnDeleteEmployee" data-id="'.$row->id.'" class="delete btn btn-danger btn-sm">Delete</button>';
	                return $actionBtn;
	            })
	            ->rawColumns(['action'])
	            ->escapeColumns([])
	            ->make(true);
	    }

	    return view($slug.'.index', compact('name','slug', 'icon', 'title'));
	}

	public function create()
    {
        $name = $this->page()["name"];
        $slug = Str::slug($this->page()["name"]);
        $icon = $this->page()["icon"];
        $title = "Add Employee Data";

        if (Auth::user()->level != "admin") {
            return redirect()->route("index")->withDanger("You dont have authorization to access this page");
        }

        $data_company = Company::orderBy("name")->get();

        return view($slug.'.create', compact('name','slug', 'icon', 'title', 'data_company'));
    }

    public function store(StoreEmployeeRequest $request)
    {
        $name = $this->page()["name"];
        $slug = Str::slug($this->page()["name"]);
        $icon = $this->page()["icon"];

        if (Auth::user()->level != "admin") {
            return redirect()->route("index")->withDanger("You dont have authorization to access this page");
        }

        $employee = new Employee;
        $employee->first_name = $request->first_name;
        $employee->last_name = $request->last_name;
        $employee->company_id = $request->company_id;
        $employee->email = $request->email;
        $employee->phone = $request->phone;

        $employee->save();

        return redirect()->route("$slug.sendNotification", ["id" => $employee->id]);
    }

    public function sendNotification($id) 
    {
        $name = $this->page()["name"];
        $slug = Str::slug($this->page()["name"]);
        $icon = $this->page()["icon"];
        
        $employee = Employee::find($id);
        $company_id = $employee->company_id;
        $company = Company::find($company_id);

        $details = [
            'greeting' => 'Hi '.$company->name,
            'body' => $employee->full_name.' has been added as an employee to your company',
            'actiontext' => 'Click here to see details',
            'actionturl' => '/',
            'lastline' => 'Thank You'
        ];

        Notification::send($company, new sendNotification($details));

        return redirect()->route("$slug.index")->withSuccess("$name has been Added Successfully");
    }

    public function edit($id)
    {
        if (Auth::user()->level != "admin") {
            return redirect()->route("index")->withDanger("You dont have authorization to access this page");
        }

        $employee = Employee::find($id);

        if (!$employee)
        {
            return response()->json([
                'success' => false,
                'msg' => "Employee not found."
            ]);
        }

        $data_company = Company::orderBy("name")->get();

        return response()->json([
            'success' => true,
            'msg' => "OK",
            'employee' => $employee,
            'company' => $data_company
        ]);
    }

    public function update(Request $request, $id)
    {
        $name = $this->page()["name"];
        $slug = Str::slug($this->page()["name"]);
        $icon = $this->page()["icon"];

        if (Auth::user()->level != "admin") {
            return redirect()->route("index")->withDanger("You dont have authorization to access this page");
        }

        $request->validate([
                            'first_name'    => 'required|max:25',
                            'last_name'     => 'required|max:25',
                            'company_id'    => 'required',
                            'email'         => 'nullable|email|max:250|unique:employees,email,' .$id,
                            'phone'         => 'nullable|unique:employees,phone,' .$id

                        ]);

        $employee = Employee::find($id);
        $employee->first_name = $request->first_name;
        $employee->last_name = $request->last_name;
        $employee->company_id = $request->company_id;
        $employee->email = $request->email;
        $employee->phone = $request->phone;

        $employee->save();

        $employee->save();
        return redirect()->route("$slug.index")->withSuccess("$name has been Edited successfully");

        
    }

    public function destroy($id)
    {
        if (Auth::user()->level != "admin") {
            return redirect()->route("index")->withDanger("You dont have authorization to access this page");
        }

        $employee = Employee::find($id);

        if (!$employee)
        {
            return response()->json([
                'success' => false,
                'msg' => "NotFound."
            ]);
        }


        $employee->delete();

        return response()->json([
            'success' => true,
            'msg' => "OK"
        ]);
    }
}


