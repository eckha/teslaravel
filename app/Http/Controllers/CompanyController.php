<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Models\Company;
use Illuminate\Http\Request;
use App\Http\Requests\StoreCompanyRequest;

use DB;
use Str;
use File;
use DataTables;


class CompanyController extends Controller
{   
    public function page()
    {
        return collect([
            "name" => "Company",
            "icon" => "fas fa-building"
        ]);
    }


    public function index(Request $request)
    {
        $name = $this->page()["name"];
        $slug = Str::slug($this->page()["name"]);
        $icon = $this->page()["icon"];
        $title = "Company Data";

        if (Auth::user()->level != "admin") {
            return redirect()->route("index")->withDanger("You dont have authorization to access this page");
        }

        if ($request->ajax()) {
            $data_company = Company::orderBy("name")->get();
            return Datatables::of($data_company)
                ->addIndexColumn()
                ->editColumn('website', function ($row) {
                        $link = '<a href="'.$row->website.'" target="blank">'.$row->website.'</a>';
                        return $link;
                })
                ->editColumn('logo', function ($row) {
                    if($row->logo && File::exists(public_path() . "/images/company/" . $row->logo)) {
                        return '<img src="images/company/'.$row->logo.'" border="0" width="100%" />';
                    } else {
                        return "";
                    }
                })
                ->addColumn('action', function($row){
                    $actionBtn = '<button id="btnEditCompany" data-id="'.$row->id.'" class="edit btn btn-success btn-sm">Edit</button> <button id="btnDeleteCompany" data-id="'.$row->id.'" class="delete btn btn-danger btn-sm">Delete</button>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->escapeColumns([])
                ->make(true);
        }

        return view($slug.'.index', compact('name','slug', 'icon', 'title'));
    }

   
    public function create()
    {
        $name = $this->page()["name"];
        $slug = Str::slug($this->page()["name"]);
        $icon = $this->page()["icon"];
        $title = "Add Company Data";

        if (Auth::user()->level != "admin") {
            return redirect()->route("index")->withDanger("You dont have authorization to access this page");
        }

        return view($slug.'.create', compact('name','slug', 'icon', 'title'));
    }

    public function store(StoreCompanyRequest $request)
    {
        $name = $this->page()["name"];
        $slug = Str::slug($this->page()["name"]);
        $icon = $this->page()["icon"];

        if (Auth::user()->level != "admin") {
            return redirect()->route("index")->withDanger("You dont have authorization to access this page");
        }

        $company = new Company;
        $company->name = $request->name;
        $company->email = $request->email;
        $company->website = $request->website;

        if ($request->hasFile("logo")) {
            $request->file("logo")->move(public_path() . "/images/company/", strtolower(str_replace(array(" ", ".", ",", "~", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "", "+", "=", "|", "/", "?", "{", "}", "[", "]", ":", ";", "'", '"'), "-", $request->name)) . "-" . date("YmdHis") . "." . $request->file("logo")->getClientOriginalExtension());
            $company->logo = strtolower(str_replace(array(" ", ".", ",", "~", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "", "+", "=", "|", "/", "?", "{", "}", "[", "]", ":", ";", "'", '"'), "-", $request->name)) . "-" . date("YmdHis") . "." . $request->file("logo")->getClientOriginalExtension();
        }

        $company->save();

        return redirect()->route("$slug.index")->withSuccess("$name has been Added Successfully");
    }

    public function show($id)
    {
        if (Auth::user()->level != "admin") {
            return redirect()->route("index")->withDanger("You dont have authorization to access this page");
        }

        $company = Company::find($id);

        if (!$company)
        {
            return response()->json([
                'success' => false,
                'msg' => "Company not found."
            ]);
        }

        return response()->json([
            'success' => true,
            'msg' => "OK",
            'company' => $company
        ]);
    }

    
    public function edit($id)
    {
        if (Auth::user()->level != "admin") {
            return redirect()->route("index")->withDanger("You dont have authorization to access this page");
        }

        $company = Company::find($id);

        if (!$company)
        {
            return response()->json([
                'success' => false,
                'msg' => "Company not found."
            ]);
        }

        return response()->json([
            'success' => true,
            'msg' => "OK",
            'company' => $company
        ]);
    }

    
    public function update(Request $request, $id)
    {
        $name = $this->page()["name"];
        $slug = Str::slug($this->page()["name"]);
        $icon = $this->page()["icon"];

        if (Auth::user()->level != "admin") {
            return redirect()->route("index")->withDanger("You dont have authorization to access this page");
        }

        $request->validate([
                            'name'     => 'required|max:250|unique:companies,name,' . $id,
                            'email'    => 'required|email|max:250|unique:companies,email,' .$id,
                            "logo"     => "nullable|file|mimes:jpg,jpeg,png,gif,webp|max:10240",
                            'website'  => 'nullable|max:250'
                        ]);

        $company = Company::find($id);
        $company->name = $request->name;
        $company->email = $request->email;
        $company->website = $request->website;

        if ($request->hasFile("logo")) {
            if ($company->logo && File::exists(public_path() . "/images/company/" . $company->logo)) {
                File::delete(public_path() . "/images/company/" . $company->logo);
            }
            $request->file("logo")->move(public_path() . "/images/company/", strtolower(str_replace(array(" ", ".", ",", "~", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "", "+", "=", "|", "/", "?", "{", "}", "[", "]", ":", ";", "'", '"'), "-", $request->name)) . "-" . date("YmdHis") . "." . $request->file("logo")->getClientOriginalExtension());
            $company->logo = strtolower(str_replace(array(" ", ".", ",", "~", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "", "+", "=", "|", "/", "?", "{", "}", "[", "]", ":", ";", "'", '"'), "-", $request->name)) . "-" . date("YmdHis") . "." . $request->file("logo")->getClientOriginalExtension();
        } else {
            if ($company->logo && File::exists(public_path() . "/images/company/" . $company->logo)) {
                rename(public_path() . "/images/company/" . $company->logo, public_path() . "/images/company/" . strtolower(str_replace(array(" ", ".", ",", "~", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "+", "=", "|", "/", "?", "{", "}", "[", "]", ":", ";", "'", '"'), "-", $request->name)) . "-" . date("YmdHis") . "." . pathinfo($company->logo, PATHINFO_EXTENSION));

                $company->logo = strtolower(str_replace(array(" ", ".", ",", "~", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "+", "=", "|", "/", "?", "{", "}", "[", "]", ":", ";", "'", '"'), "-", $request->name)) . "-" . date("YmdHis") . "." . pathinfo($company->logo, PATHINFO_EXTENSION);
            }
        }

        $company->save();
        return redirect()->route("$slug.index")->withSuccess("$name has been Edited successfully");

        
    }

    
    public function destroy($id)
    {
        if (Auth::user()->level != "admin") {
            return redirect()->route("index")->withDanger("You dont have authorization to access this page");
        }

        $company = Company::find($id);

        if (!$company)
        {
            return response()->json([
                'success' => false,
                'msg' => "NotFound."
            ]);
        }

        if ($company->logo && File::exists(public_path() . "/images/company/" . $company->logo)) {
            File::delete(public_path() . "/images/company/" . $company->logo);
        }

        $company->delete();

        return response()->json([
            'success' => true,
            'msg' => "OK"
        ]);
    }
}
