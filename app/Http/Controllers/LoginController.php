<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Str;

class LoginController extends Controller
{
    public function page()
    {
        return collect([
            "name" => "Login",
            "icon" => "fas fa-th-sign-in-alt"
        ]);
    }

    public function index(Request $request)
    {
        $name = $this->page()["name"];
        $slug = Str::slug($this->page()["name"]);
        $icon = $this->page()["icon"];

        if ($request->isMethod("post")) {

            $credentials = $request->validate([
	            'email' => ['required', 'email'],
	            'password' => ['required'],
	        ]);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return redirect()->intended("")->withSuccess("Login has been Successfully");
        }

        return redirect()->back()->withInput()->withDanger("Username or Password is invalid");

        }

        return view("$slug.list", [
            "name" => $name,
            "slug" => $slug,
            "icon" => $icon
        ]);
    }

}
