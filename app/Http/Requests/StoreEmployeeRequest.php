<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'first_name'    => 'required|max:25',
                'last_name'     => 'required|max:25',
                'company_id'    => 'required',
                'email'         => 'nullable|email|max:250|unique:employees,email',
                'phone'         => 'nullable|unique:employees,phone'
            ];
    }

    public function messages()
    {
        return [
            'first_name.required'     => 'First name required',
            'first_name.max'          => 'First name max 25 character',

            'last_name.required'     => 'Last name required',
            'last_name.max'          => 'Last name max 25 character',

            'company_id.required'     => 'Company required',

            'email.email'           => 'Wrong email format',
            'email.unique'          => 'Email already used',

            'phone.unique'          => 'Phone already used',

        ];
    }
}
