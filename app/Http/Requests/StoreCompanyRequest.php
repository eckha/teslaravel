<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'     => 'required|max:250|unique:companies,name',
            'email'    => 'required|email|max:250|unique:companies,email',
            "logo"     => "nullable|file|mimes:jpg,jpeg,png,gif,webp|max:10240",
            'website'  => 'nullable|max:250'
        ];
    }

    public function messages()
    {
        return [
            'name.required'         => 'Company name required',
            'name.max'              => 'Company name max 250 character',
            'name.unique'           => 'Company name already used',

            'email.email'           => 'Wrong email format',
            'email.unique'          => 'Email already used',

            "logo.file"             => "Logo must be a file",
            "logo.mimes"            => "Logo must be in : .jpg .jpeg .png .gif .webp",
            "logo.max"              => "Logo : Max Size 10mb",

            'website.max'           => 'website max 250 character',
        ];
    }
}
